﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {
    public GameObject BMG;

	void Update () {
        Vector3 BMGPos = BMG.transform.position;

        //カメラとプレイヤーの位置を同じにする
        transform.position = new Vector3(BMGPos.x, 0, -10);
    }
}
