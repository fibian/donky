﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BMGController : MonoBehaviour
{

    Rigidbody2D rb;
    Animator animator;

    float jumpForce = 2.0f; //ジャンプ中に加える力
    float jumpState =1.0f; //ジャンプしているかの判定
    float runForce= 0.05f; //走る力
    float runSpeed = 0.01f; //走っている間の速度
    float runState; //速度切り替え判定
    bool isGround  =true; //設置判定
    int key = 0; //左右の入力管理

    string state;  //プレイヤーの状態
    string prevState;  //前の状態


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();

    }
    void Update()
    {
        GetInputKey();
        ChangeState();
        ChangeAnimation();
        Move();
    }
    void GetInputKey()
    {
            key = 0;
        if (Input.GetKey("left"))
            key = -1;
        if (Input.GetKey("right"))
            key = 1;
    }
        
    void ChangeState()
    {
        if (Mathf.Abs(rb.velocity.y) > jumpState)
        {
            isGround = false;
        }

        if (isGround)
            {
                if (key != 0)
                {
                    state = "WALK";
                }
                else
                {
                state = "FLAP";
                }
            }
        else
        {
            state = "JUMP";
        }
        
    }
    void ChangeAnimation()
    {
        // 状態が変わった場合のみアニメーションを変更する
        if (prevState != state)
        {
            switch (state)
            {
                case "JUMP":
                    animator.SetBool("isJump", true); ;
                    animator.SetBool("isWalk", false);
                    animator.SetBool("isFlap", false);
                    animator.SetBool("isDown", false);
                    break;

                case "WALK":
                    animator.SetBool("isWalk", true);
                    animator.SetBool("isJump", false);
                    animator.SetBool("isFlap", false);
                    animator.SetBool("isDown", false);

                    transform.localScale = new Vector3(key, 1, 1); // 向きに応じてキャラクターのspriteを反転
                    break;

                /*case "Down":
                    animator.SetBool("isDown", true);
                    animator.SetBool("isWalk", false);
                    animator.SetBool("isJump", false);
                    animator.SetBool("isFlap", false);*/

                default:
                    animator.SetBool("isFlap", true);
                    animator.SetBool("isWalk", false);
                    animator.SetBool("isJump", false);
                    animator.SetBool("isDown", false);
                    //stateEffect = 1f;
                    break;
            }
            // 状態の変更を判定するために状態を保存しておく
            prevState = state;
        }
    }
        void Move()
        {
        if (isGround)
        {
            if (Input.GetButton("Jump"))
            {
                rb.AddForce(transform.up * jumpForce);
                isGround = false;
            }

            // 左右の移動
            // rb.AddForce(transform.right * key * runForce );
            transform.position += new Vector3(runSpeed * key, 0, 0);
        
            }
    
        }

        // 着地判定
        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.tag == "Ground")
            {
                if (!isGround)
                    isGround = true;
            }
        }
        void OnTriggerStay2D(Collider2D other)
        {
            if (other.gameObject.tag == "Ground")
            {
                if (!isGround)
                    isGround = true;
            }
        }
}

 

